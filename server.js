//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

 var movimientosJSONv2 = require("./movimientosv2.json");

 console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req, res) {
  //res.send("hola mundo desde node js");
  res.sendFile(path.join(__dirname,'index.html'));
})

app.post('/',function(req, res){
  res.send("hemos recibido su peticion post");
})

app.put('/',function(req, res){
  res.send("hemos recibido su peticion put");
})

app.delete('/',function(req, res){
  res.send("hemos recibido su peticion delete");
})

app.get('/Clientes',function (req, res) {
  res.send("Aqui tiene a los clientes");
})

app.get('/Clientes/:idcliente',function (req, res) {
  res.send("Aqui tiene al cliente: "+ req.params.idcliente);
})

app.get('/v1/movimientos',function (req, res) {
  //res.send("Aqui tiene los movimientos: ");
    res.sendfile("movimientosv1.json");
})

app.get('/v2/movimientos',function (req, res) {
    res.json(movimientosJSONv2);
})

app.get('/v2/movimientos/:id',function (req, res) {
    console.log(req.params.id);
    res.send(movimientosJSONv2[req.params.id - 1]);
})

app.get('/v2/movimientosq',function (req, res) {
    console.log(req.query);
    res.send("Peticion con query resivida imagen automatica: "+ req.query);
})


app.get('/v2/movimientos/:id/:nombre',function (req, res) {
    console.log(req.params);
    console.log(req.params.id);
    console.log(req.params.nombre);
    res.send("Resiviendo arios parametros:");
})
